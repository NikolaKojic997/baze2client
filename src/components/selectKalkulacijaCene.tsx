import axios from 'axios';
import React from 'react';
// @ts-ignore
// import DatePicker from "react-datepicker";
import { Button, Dropdown, Form, Input, Modal, Table } from "semantic-ui-react";
import AddKalkulacijaCene from './addKalkulacijaCene';
import AddStavkaInternaOtpremnica from './AddStavkaInterneOtpremnice';


export type Kalkulacija = {
    sifraKalkulacije: number,
    datumDPO: Date,
    datumPrijemaDobra: Date,
    datumValute: Date,
    datumDokumenta: Date,
    sifraDobavljaca: number,
    sifraSkladista: number
}

interface Otpremnica {
    sifraOtpremnice: number;
    datumDokumenta: Date;
    sifraSkladista: number;
    sifraMaloprodaje: number;
    sifraOtpreme: number;
    brojRadneKnjizice: string;
    ukupno: number;
}

interface IProps {

}

interface DropDownItem {
    key: number,
    value: number,
    text: string
}

export type Stavka = {
    redniBroj: number,
    sifraOtpremnice: number,
    datumDokumenta?: Date,
    proizvod?: string,
    kolicina: number,
    cena?: number,
    pdv?: number,
    ukupno?: number,
    rabat?: number,
    sifraProizvoda?: number,
    sifraPDV?: number
}

interface IState {
    selectedOtpremnica: number,
    kalkulacije: Kalkulacija[],
    activeItem: number,
    modalAddOpen: boolean,
    modalUpdateOpen: boolean
}
class SelectKalkulacijaCene extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            kalkulacije: [],
            activeItem: 0,
            modalAddOpen: false,
            modalUpdateOpen: false,
            selectedOtpremnica: 0
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdateKalkulacija = this.handleUpdateKalkulacija.bind(this);
        this.handleAddKalkulacija = this.handleAddKalkulacija.bind(this);
        this.dropdownChangeOtpremnica = this.dropdownChangeOtpremnica.bind(this);
    }

    async componentDidMount() {

        await axios.get(`http://localhost:4000/kalkulacija`)
            .then(async res => {
                console.log(res.data)
                await this.setState({
                    kalkulacije: res.data
                })
            })
            .catch(error => {
                console.log(error)
        });
        
    }

    handleClick = (id: number) => {
        this.setState({
            ...this.state,
            activeItem: id
        });
    }


    handleAddKalkulacija = (k: Kalkulacija) => {
        this.setState({
            modalAddOpen: false,
            kalkulacije: [...this.state.kalkulacije, k]
        })
    }
    handleUpdateKalkulacija = (k: Kalkulacija) => {
        this.setState({
            modalUpdateOpen: false,
            kalkulacije: this.state.kalkulacije.filter( obj => obj.sifraKalkulacije !== k.sifraKalkulacije)
        })
        this.setState({
            kalkulacije: [...this.state.kalkulacije, k]
        })
    }

    handleDelete = async () => {
        let activeItem = this.state.activeItem;
        await axios.delete(`http://localhost:4000/kalkulacija/${activeItem}`)
            .then(res => {
                console.log(res);
                alert("Kalkulacija obrisana")
            })
            .catch(err => {
                console.log(err)
            })
        this.setState(
            {
                kalkulacije: this.state.kalkulacije.filter(obj => obj.sifraKalkulacije !== activeItem),
                activeItem: 0
            }
        )
    }

    dropdownChangeOtpremnica =async  (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        
    }

    render() {

        return (
            <div>
                <Table celled selectable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Sifra kalkulacije</Table.HeaderCell>
                            <Table.HeaderCell>Datum DPO</Table.HeaderCell>
                            <Table.HeaderCell>Datum prijema dobra</Table.HeaderCell>
                            <Table.HeaderCell>Datum valute</Table.HeaderCell>
                            <Table.HeaderCell>Datum dokumenta</Table.HeaderCell>
                            <Table.HeaderCell>Sifra dobavljaca</Table.HeaderCell>
                            <Table.HeaderCell>Sifra skladista</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.kalkulacije.map(element => (

                            <Table.Row key={element.sifraKalkulacije} active={this.state.activeItem == element.sifraKalkulacije}
                                onClick={() => {
                                    this.handleClick(element.sifraKalkulacije)
                                }}>
                                <Table.Cell>{element.sifraKalkulacije}</Table.Cell>
                                <Table.Cell>{element.datumDPO}</Table.Cell>
                                <Table.Cell>{element.datumPrijemaDobra}</Table.Cell>
                                <Table.Cell>{element.datumValute}</Table.Cell>
                                <Table.Cell>{element.datumDokumenta}</Table.Cell>
                                <Table.Cell>{element.sifraDobavljaca}</Table.Cell>
                                <Table.Cell>{element.sifraSkladista}</Table.Cell>
                            </Table.Row>


                        ))}

                    </Table.Body>
                </Table>
                <div>
                    <Button onClick={() => this.setState({ modalAddOpen: true })}>Add new</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={() => this.setState({ modalUpdateOpen: true })}>Update</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={this.handleDelete}>Delete</Button>
                </div>
                  <Modal
                    open={this.state.modalAddOpen}
                    onClose={()=> this.setState({
                        modalAddOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Add Kalkulacija</Modal.Header>
                    <Modal.Content>
                        <AddKalkulacijaCene activeKalkulacija={undefined} closeModal={this.handleAddKalkulacija} />
                    </Modal.Content>
                </Modal> 
                <Modal
                    open={this.state.modalUpdateOpen}
                    onClose={()=> this.setState({
                        modalUpdateOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Update User</Modal.Header>
                    <Modal.Content>
                        <AddKalkulacijaCene activeKalkulacija = {this.state.kalkulacije.find(t => t.sifraKalkulacije === this.state.activeItem)} closeModal={this.handleUpdateKalkulacija}/>
                    </Modal.Content>
                </Modal>                       
            </div>
        );
    }
}

export default SelectKalkulacijaCene;