import React from 'react';
//@ts-ignore
import DatePicker from "react-datepicker";
import { Button, Dropdown, Form, Input } from "semantic-ui-react";
import { dbBrocker } from '../db/db-brocker';
import { Maloprodaja } from '../domain/maloprodaja';
import { Skladiste } from '../domain/skladiste';
import SecondaryMenuInternaOtpremnica from './InternaOtpremnicaSecundary';
import axios from 'axios'
import { Otprema } from '../domain/otprema';
import { InternaOtpremnica } from '../domain/internaOtpremnica';
import { throws } from 'node:assert';
import { Kalkulacija } from './selectKalkulacijaCene';

export type Dobavljac = {
    sifraDobavljaca: number,
    nazivDobavljaca: string
}

interface Otpremnica  {
    sifraOtpremnice: number;
    datumDokumenta: Date;
    sifraSkladista: number;
    sifraMaloprodaje: number;
    sifraOtpreme: number;
    brojRadneKnjizice: string;
    ukupno: number;
}

interface IProps {
    closeModal: any,
    activeKalkulacija: Kalkulacija | undefined
}

interface DropDownItem {
    key: number,
    value: number,
    text: string
}
interface IState {
    SifraKalkulacije: number,
    datumDokumenta: any,
    datumValute: any,
    datumDPO: any,
    datumPrijema: any,
    identificationNumber: string,
    message: string,
    skladista: DropDownItem[],
    dobavljaci: DropDownItem[],
    selectedSkladiste: number,
    selectedDobavljac: number,
    otpreme: DropDownItem[],
    selectedOtprema: number
}


class AddKalkulacijaCene extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            datumDPO: new Date(),
            datumPrijema: new Date(),
            datumValute: new Date(),
            otpreme: [],
            selectedOtprema: 0,
            skladista: [],
            selectedSkladiste:  0,
            selectedDobavljac:0,
            dobavljaci: [],
            SifraKalkulacije:0,
            datumDokumenta: new Date(),
            identificationNumber: '',
            message: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitAddOtpremnica = this.handleSubmitAddOtpremnica.bind(this);
        this.dropdownChangeSkladiste = this.dropdownChangeSkladiste.bind(this);
        this.dropdownChangeDobavljac = this.dropdownChangeDobavljac.bind(this);
        this.dropdownChangeOtprema = this.dropdownChangeOtprema.bind(this);


    }

    dropdownChangeOtprema = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedOtprema: data.value })
    }

    dropdownChangeSkladiste = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedSkladiste: data.value })
    }

    dropdownChangeDobavljac = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedDobavljac: data.value })
    }
    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        });
    }

    private handleSubmitAddOtpremnica = async (
        e: React.FormEvent<HTMLFormElement>
    ): Promise<void> => {
        let config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        let data: Kalkulacija = {
            sifraKalkulacije: this.state.SifraKalkulacije,
            datumDPO: this.state.datumDPO,
            datumDokumenta: this.state.datumDokumenta,
            datumPrijemaDobra: this.state.datumPrijema,
            datumValute: this.state.datumValute,
            sifraDobavljaca: this.state.selectedDobavljac,
            sifraSkladista: this.state.selectedSkladiste
        }

        if(!this.props.activeKalkulacija)
        {
            
        await axios.post('http://localhost:4000/kalkulacija', data, config)
            .then(async res =>  {
                alert("Kalkulacija uspesno dodata!")
                let o = await axios.get(`http://localhost:4000/kalkulacija/${data.sifraKalkulacije}`)
                this.props.closeModal(o.data);
            })
            .catch(error => {
                alert("GRESKA!")
            })

        }
        else {
            await axios.put('http://localhost:4000/kalkulacija', data, config)
            .then(async res =>  {
                alert("kalkulacija uspesno izmenjena!")
                let o = await axios.get(`http://localhost:4000/kalkulacija/${data.sifraKalkulacije}`)
                this.props.closeModal(o.data);
            })
            .catch(error => {
                alert("GRESKA!")
            })
        }
};



    async componentDidMount() {

        if(this.props.activeKalkulacija){
            this.setState({
                SifraKalkulacije: this.props.activeKalkulacija.sifraKalkulacije,
                datumDPO: new Date(this.props.activeKalkulacija.datumDPO),
                datumDokumenta: new Date(this.props.activeKalkulacija.datumDokumenta),
                datumPrijema: new Date(this.props.activeKalkulacija.datumPrijemaDobra),
                datumValute: new Date(this.props.activeKalkulacija.datumValute),
                selectedDobavljac: this.props.activeKalkulacija.sifraDobavljaca,
                selectedSkladiste: this.props.activeKalkulacija.sifraSkladista
            })
        }

        await axios.get('http://localhost:4000/skladiste')
            .then(res => {
                console.log(res.data)
                this.setState({
                    skladista: res.data.map((element: Skladiste) =>
                    ({
                        value: element.sifraSkladista,
                        text: element.imeSkladista,
                        key: element.sifraSkladista
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });
        await axios.get('http://localhost:4000/dobavljac')
            .then(res => {
                console.log(res.data)
                this.setState({
                    dobavljaci: res.data.map((element: Dobavljac) =>
                    ({
                        value: element.sifraDobavljaca,
                        text: element.nazivDobavljaca,
                        key: element.sifraDobavljaca
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });

        
    }



    render() {

        return (
            <div>
                <Form id='AddOtpremnicaForm' onSubmit={this.handleSubmitAddOtpremnica}>
                    <Form.Field>
                        <label>Sifra Kalkulacije</label>
                        <Input icon='user outline' iconPosition='left' placeholder='SifraKalkulacije' name='SifraKalkulacije' value={this.state.SifraKalkulacije} onChange={this.handleChange} />
                    </Form.Field>
                    <Form.Field>
                        <label>Datum DPO</label>
                        <DatePicker
                            dateFormat='dd-MM-yyyy'
                            selected={this.state.datumDPO}
                            onChange={(date: any) => this.setState({ datumDPO: date })}
                            placeholderText="Click to select date..."
                            maxDate={new Date()}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Datum prijema robe</label>
                        <DatePicker
                            dateFormat='dd-MM-yyyy'
                            selected={this.state.datumPrijema}
                            onChange={(date: any) => this.setState({ datumPrijema: date })}
                            placeholderText="Click to select date..."
                            maxDate={new Date()}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Datum valute</label>
                        <DatePicker
                            dateFormat='dd-MM-yyyy'
                            selected={this.state.datumValute}
                            onChange={(date: any) => this.setState({ datumValute: date })}
                            placeholderText="Click to select date..."
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Datum dokumenta</label>
                        <DatePicker
                            dateFormat='dd-MM-yyyy'
                            selected={this.state.datumDokumenta}
                            onChange={(date: any) => this.setState({ datumDokumenta: date })}
                            placeholderText="Click to select date..."
                            maxDate={new Date()}
                        />
                    </Form.Field>

                    <Form.Field>
                        <label>Dobavljac</label>
                        <Dropdown
                            placeholder='Izaberite dobavljaca..'
                            fluid
                            selection
                            options={this.state.dobavljaci}
                            name="value"
                            value={this.state.selectedDobavljac}
                            onChange={this.dropdownChangeDobavljac}
                        >
                        </Dropdown>
                    </Form.Field>
                    <Form.Field>
                        <label>Skladiste</label>
                        <Dropdown
                            placeholder='Izaberite skladiste..'
                            fluid
                            selection
                            options={this.state.skladista}
                            name="value"
                            value={this.state.selectedSkladiste}
                            onChange={this.dropdownChangeSkladiste}
                        >
                        </Dropdown>
                    </Form.Field>

                    
                    <Button id="loginBtn" primary type='submit'>Submit</Button>
                </Form>

            </div >
        );
    }
}

export default AddKalkulacijaCene;