import axios from 'axios';
import React from 'react';
// @ts-ignore
// import DatePicker from "react-datepicker";
import { Button, Dropdown, Form, Input, Modal, Table } from "semantic-ui-react";
import AddInternaOtpremnica from './AddInternaOtpremnica';
import AddStavkaInternaOtpremnica from './AddStavkaInterneOtpremnice';
import SecondaryMenuInternaOtpremnica from './InternaOtpremnicaSecundary';




interface Otpremnica {
    sifraOtpremnice: number;
    datumDokumenta: Date;
    sifraSkladista: number;
    sifraMaloprodaje: number;
    sifraOtpreme: number;
    brojRadneKnjizice: string;
    ukupno: number;
}

interface IProps {

}

interface DropDownItem {
    key: number,
    value: number,
    text: string
}

export type Stavka = {
    redniBroj: number,
    sifraOtpremnice: number,
    datumDokumenta?: Date,
    proizvod?: string,
    kolicina: number,
    cena?: number,
    pdv?: number,
    ukupno?: number,
    rabat?: number,
    sifraProizvoda?: number,
    sifraPDV?: number
}

interface IState {
    otpremnice: DropDownItem[],
    selectedOtpremnica: number,
    stavke: Stavka[],
    activeItem: number,
    modalAddOpen: boolean,
    modalUpdateOpen: boolean

}
class SelectStavkaInterneOtpremnice extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            stavke: [],
            otpremnice: [],
            activeItem: 0,
            modalAddOpen: false,
            modalUpdateOpen: false,
            selectedOtpremnica: 0
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdateStavka = this.handleUpdateStavka.bind(this);
        this.handleAddOtpremnica = this.handleAddOtpremnica.bind(this);
        this.dropdownChangeOtpremnica = this.dropdownChangeOtpremnica.bind(this);
    }

    async componentDidMount() {

        await axios.get('http://localhost:4000/otpremnica')
            .then(res => {
                console.log(res.data)
                this.setState({
                    otpremnice: res.data.map((element: Otpremnica) =>
                    ({
                        value: element.sifraOtpremnice,
                        text: element.sifraOtpremnice,
                        key: element.sifraOtpremnice
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });




    }

    handleClick = (id: number) => {
        this.setState({
            ...this.state,
            activeItem: id
        });
    }


    handleAddOtpremnica = (s: Stavka) => {
        this.setState({
            modalAddOpen: false,
            stavke: [...this.state.stavke, s]
        })
    }
    handleUpdateStavka = (s: Stavka) => {
        this.setState({
            modalUpdateOpen: false,
            stavke: this.state.stavke.filter( obj => obj.redniBroj !== s.redniBroj)
        })
        this.setState({
            stavke: [...this.state.stavke, s]
        })
    }

    handleDelete = async () => {
        let activeItem = this.state.activeItem;
        await axios.delete(`http://localhost:4000/stavka/${this.state.selectedOtpremnica}/${activeItem}`)
            .then(res => {
                console.log(res);
                alert("Otpremnica obrisana")
            })
            .catch(err => {
                console.log(err)
            })
        this.setState(
            {
                stavke: this.state.stavke.filter(obj => obj.redniBroj !== activeItem),
                activeItem: 0
            }
        )
    }

    dropdownChangeOtpremnica =async  (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        await this.setState({
            ...this.state,
            selectedOtpremnica: data.value
        })
        await axios.get(`http://localhost:4000/stavka/${this.state.selectedOtpremnica}`)
            .then(async res => {
                console.log(res.data)
                await this.setState({
                    stavke: res.data
                })
            })
            .catch(error => {
                console.log(error)
            });
    }

    render() {

        return (
            <div>
                <Form.Field>
                    <label>Sifra interne otpremnice</label>
                    <Dropdown
                        placeholder = 'Izaberite otpremnicu..'
                        fluid
                        selection
                        options={this.state.otpremnice}
                        name="value"
                        value={this.state.selectedOtpremnica}
                        onChange={ this.dropdownChangeOtpremnica}
                    >
                    </Dropdown>
                </Form.Field>
                <Table celled selectable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Redni broj stavke</Table.HeaderCell>
                            <Table.HeaderCell>Datum dokumenta</Table.HeaderCell>
                            <Table.HeaderCell>Proizvod</Table.HeaderCell>
                            <Table.HeaderCell>Kolicina</Table.HeaderCell>
                            <Table.HeaderCell>Cena</Table.HeaderCell>
                            <Table.HeaderCell>Pdv</Table.HeaderCell>
                            <Table.HeaderCell>Rabat</Table.HeaderCell>
                            <Table.HeaderCell>Ukupno</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.stavke.map(element => (

                            <Table.Row key={element.redniBroj} active={this.state.activeItem == element.redniBroj}
                                onClick={() => {
                                    this.handleClick(element.redniBroj)
                                }}>
                                <Table.Cell>{element.redniBroj}</Table.Cell>
                                <Table.Cell>{element.datumDokumenta}</Table.Cell>
                                <Table.Cell>{element.proizvod}</Table.Cell>
                                <Table.Cell>{element.kolicina}</Table.Cell>
                                <Table.Cell>{element.cena}</Table.Cell>
                                <Table.Cell>{element.pdv}</Table.Cell>
                                <Table.Cell>{element.rabat}</Table.Cell>
                                <Table.Cell>{element.ukupno}</Table.Cell>
                            </Table.Row>


                        ))}

                    </Table.Body>
                </Table>
                <div>
                    <Button onClick={() => this.setState({ modalAddOpen: true })}>Add new</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={() => this.setState({ modalUpdateOpen: true })}>Update</Button>
                    <Button disabled={this.state.activeItem === 0} onClick={this.handleDelete}>Delete</Button>
                </div>
                 <Modal
                    open={this.state.modalAddOpen}
                    onClose={()=> this.setState({
                        modalAddOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Add User</Modal.Header>
                    <Modal.Content>
                        <AddStavkaInternaOtpremnica rb = {this.state.stavke.length +1} otpremnica ={this.state.selectedOtpremnica} activeStavka={undefined} closeModal={this.handleAddOtpremnica} />
                    </Modal.Content>
                </Modal>
                 <Modal
                    open={this.state.modalUpdateOpen}
                    onClose={()=> this.setState({
                        modalUpdateOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Update User</Modal.Header>
                    <Modal.Content>
                        <AddStavkaInternaOtpremnica rb = {this.state.activeItem} otpremnica = {this.state.selectedOtpremnica}  activeStavka= {this.state.stavke.find(t => t.redniBroj === this.state.activeItem)} closeModal={this.handleUpdateStavka}/>
                    </Modal.Content>
                </Modal>                      
            </div>
        );
    }
}

export default SelectStavkaInterneOtpremnice;