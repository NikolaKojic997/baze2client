import React, { Component } from 'react'
import { Input, Menu } from 'semantic-ui-react'
const useHistory = require("react-router-dom").useHistory

interface IProps {
    activeItem: string
}

function SecondaryMenuInternaOtpremnica(props:IProps) {

    let history = useHistory();
    return (
      <Menu secondary>
        <Menu.Item
          name='insert'
          active={props.activeItem === 'insert'}
          onClick = {()=> {
            history.push('/InternaOtpremnica/insert')
          }}
          
        />
        <Menu.Item
          name='select'
          active={props.activeItem === 'select'}
          onClick = {()=> {
            history.push('/InternaOtpremnica/select')
          }}
        />
      </Menu>
    )
  
}

export default SecondaryMenuInternaOtpremnica;