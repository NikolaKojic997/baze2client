import React, { Component } from 'react'
import {Button, Menu} from 'semantic-ui-react'
import {log} from "util";
const Link = require("react-router-dom").Link
const useHistory = require("react-router-dom").useHistory


interface IProps {
    activeItem: string
    handleLogOut: any
}

interface IState {

}


function PageHeader(props:IProps)  {

    function logOut(){
        props.handleLogOut();
        history.push('/')
    }

     let history = useHistory();

     return (

            <Menu>
                <Menu.Item
                    name='Interna Otpremnica'
                    active={props.activeItem === 'InternaOtpremnica'}
                    onClick ={() => {
                        history.push('/InternaOtpremnica')
                    }}
                >
                    Interna Otpremnica
                </Menu.Item>
                <Menu.Item
                    name='StavkaInterneOtpremnice'
                    active={props.activeItem === 'StavkaInterneOtpremnice'}
                    onClick ={() => {
                        history.push('/StavkaInterneOtpremnice')
                    }}
                >
                    Stavka Interne Otpremnice
                </Menu.Item>
                <Menu.Item
                    name='kalkulacijaCene'
                    active={props.activeItem === 'kalkulacijaCene'}
                    onClick ={() => {
                        history.push('/kalkulacijaCene')
                    }}
                >
                    Kalkulacija Cene
                </Menu.Item>

                {/* <Menu.Item
                    name='Assistents'
                    active={props.activeItem === 'Assistents'}
                    onClick ={() => {
                        history.push('/Assistents')
                    }}
                >
                    Assistents
                </Menu.Item>

                <Menu.Item
                    name='Teachers'
                    active={props.activeItem === 'Teachers'}
                    onClick ={() => {
                        history.push('/Teachers')
                    }}
                 >
                    Teachers
                </Menu.Item>

                <Menu.Item
                    name='DeleteAcc'
                    onClick ={() => {
                        history.push('/DeleteAcc')
                    }}

                >
                    Delete profile
                </Menu.Item>

                <Menu.Item
                    name='LogOut'
                    position={'right'}
                    active={props.activeItem === 'LogOut'}
                    onClick={()=> logOut()}
                >
                    Log out
                </Menu.Item> */}

            </Menu>
    )

}

export default PageHeader;