import axios from 'axios';
import React from 'react';
// @ts-ignore
// import DatePicker from "react-datepicker";
import { Button, Form, Input, Modal, Table } from "semantic-ui-react";
import AddInternaOtpremnica from './AddInternaOtpremnica';
import SecondaryMenuInternaOtpremnica from './InternaOtpremnicaSecundary';




interface Otpremnica  {
    sifraOtpremnice: number;
    datumDokumenta: Date;
    sifraSkladista: number;
    sifraMaloprodaje: number;
    sifraOtpreme: number;
    brojRadneKnjizice: string;
    ukupno: number;
}

interface IProps {

}

interface IState {
    otpremnice: Otpremnica[],
    activeItem: number,
    modalAddOpen: boolean,
    modalUpdateOpen: boolean

}
class SelectInternaOtpremnica extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state ={
            otpremnice: [],
            activeItem: 0,
            modalAddOpen: false,
            modalUpdateOpen: false
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdateOtpremnica = this.handleUpdateOtpremnica.bind(this);
        this.handleAddOtpremnica = this.handleAddOtpremnica.bind(this);
    }

    async componentDidMount(){

        await axios.get('http://localhost:4000/otpremnica')
            .then(res=> {
                console.log(res.data)
                this.setState({
                    otpremnice: res.data
                })
            })
            .catch(error => {
                console.log(error)
            });

    }

    handleClick = (id: number)=> {
        this.setState({
            activeItem:id
        });
    }


    handleAddOtpremnica = (o: Otpremnica) =>{
        this.setState({
            modalAddOpen: false,
            otpremnice: [...this.state.otpremnice, o]
        })
    }
    handleUpdateOtpremnica = (o: Otpremnica) =>{
        this.setState({
            modalUpdateOpen: false,
            otpremnice: this.state.otpremnice.filter( obj => obj.sifraOtpremnice !== o.sifraOtpremnice)
        })
        this.setState({
            otpremnice: [...this.state.otpremnice, o]
        })
    }

    handleDelete = async ()=> {
        let activeItem = this.state.activeItem;
        await axios.delete(`http://localhost:4000/otpremnica/${activeItem}`)
            .then(res => {
                console.log(res);
                alert("Otpremnica obrisana")
            })
            .catch(err => {
                console.log(err)
            })
        this.setState(
            {
                otpremnice: this.state.otpremnice.filter(obj => obj.sifraOtpremnice !== activeItem),
                activeItem: 0
            }
        )
    }


    render() {

        return (
            <div>
                <Table celled selectable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Sifra otpremnice</Table.HeaderCell>
                            <Table.HeaderCell>Sifra otpreme</Table.HeaderCell>
                            <Table.HeaderCell>Broj radne knjizice</Table.HeaderCell>
                            <Table.HeaderCell>Datum dokumenta</Table.HeaderCell>
                            <Table.HeaderCell>Ukupno</Table.HeaderCell>
                            <Table.HeaderCell>Sifra skladista</Table.HeaderCell>
                            <Table.HeaderCell>Sifra maloprodaje</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.otpremnice.map(element => (

                            <Table.Row key={element.sifraOtpremnice} active={this.state.activeItem == element.sifraOtpremnice}
                                onClick={() => {
                                    this.handleClick(element.sifraOtpremnice)
                                    console.log(this.state.activeItem)
                                    console.log(this.state.otpremnice.find(t => t.sifraOtpremnice === this.state.activeItem))
                                }}>
                                <Table.Cell>{element.sifraOtpremnice}</Table.Cell>
                                <Table.Cell>{element.sifraOtpreme}</Table.Cell>
                                <Table.Cell>{element.brojRadneKnjizice}</Table.Cell>
                                <Table.Cell>{element.datumDokumenta}</Table.Cell>
                                <Table.Cell>{element.ukupno}</Table.Cell>
                                <Table.Cell>{element.sifraSkladista}</Table.Cell>
                                <Table.Cell>{element.sifraMaloprodaje}</Table.Cell>
                            </Table.Row>


                        ))}

                    </Table.Body>
                </Table>
                <div>
                <Button onClick={()=> this.setState({modalAddOpen:true})}>Add new</Button>
                <Button disabled={this.state.activeItem === 0} onClick={()=> this.setState({modalUpdateOpen:true})}>Update</Button>
                <Button disabled={this.state.activeItem === 0} onClick={this.handleDelete}>Delete</Button>
            </div>
                <Modal
                    open={this.state.modalAddOpen}
                    onClose={()=> this.setState({
                        modalAddOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Add User</Modal.Header>
                    <Modal.Content>
                        <AddInternaOtpremnica activeOtpremnica={undefined} closeModal={this.handleAddOtpremnica} />
                    </Modal.Content>
                </Modal>
                <Modal
                    open={this.state.modalUpdateOpen}
                    onClose={()=> this.setState({
                        modalUpdateOpen: false
                    })}
                    closeIcon>
                    <Modal.Header>Update User</Modal.Header>
                    <Modal.Content>
                        <AddInternaOtpremnica activeOtpremnica= {this.state.otpremnice.find(t => t.sifraOtpremnice === this.state.activeItem)} closeModal={this.handleUpdateOtpremnica}/>
                    </Modal.Content>
                </Modal>                    
            </div>
        );
    }
}

export default SelectInternaOtpremnica;