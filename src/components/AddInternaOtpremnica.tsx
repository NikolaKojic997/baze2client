import React from 'react';
//@ts-ignore
import DatePicker from "react-datepicker";
import { Button, Dropdown, Form, Input } from "semantic-ui-react";
import { dbBrocker } from '../db/db-brocker';
import { Maloprodaja } from '../domain/maloprodaja';
import { Skladiste } from '../domain/skladiste';
import SecondaryMenuInternaOtpremnica from './InternaOtpremnicaSecundary';
import axios from 'axios'
import { Otprema } from '../domain/otprema';
import { InternaOtpremnica } from '../domain/internaOtpremnica';
import { throws } from 'node:assert';

interface Otpremnica  {
    sifraOtpremnice: number;
    datumDokumenta: Date;
    sifraSkladista: number;
    sifraMaloprodaje: number;
    sifraOtpreme: number;
    brojRadneKnjizice: string;
    ukupno: number;
}

interface IProps {
    closeModal: any,
    activeOtpremnica: Otpremnica | undefined
}

interface DropDownItem {
    key: number,
    value: number,
    text: string
}
interface IState {
    sifraOtpremnice: number,
    datumDokumenta: any,
    datumValute: any,
    identificationNumber: string,
    message: string,
    skladista: DropDownItem[],
    maloprodaje: DropDownItem[],
    selectedSkladiste: number,
    selectedMaloprodaja: number,
    otpreme: DropDownItem[],
    selectedOtprema: number
}


class AddInternaOtpremnica extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            datumValute: '',
            otpreme: [],
            selectedOtprema: 0,
            skladista: [],
            selectedSkladiste:  0,
            selectedMaloprodaja:0,
            maloprodaje: [],
            sifraOtpremnice:0,
            datumDokumenta: new Date(),
            identificationNumber: '',
            message: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitAddOtpremnica = this.handleSubmitAddOtpremnica.bind(this);
        this.dropdownChangeSkladiste = this.dropdownChangeSkladiste.bind(this);
        this.dropdownChangeMaloprodaja = this.dropdownChangeMaloprodaja.bind(this);
        this.dropdownChangeOtprema = this.dropdownChangeOtprema.bind(this);


    }

    dropdownChangeOtprema = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedOtprema: data.value })
    }

    dropdownChangeSkladiste = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedSkladiste: data.value })
    }

    dropdownChangeMaloprodaja = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedMaloprodaja: data.value })
    }
    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        });
    }

    private handleSubmitAddOtpremnica = async (
        e: React.FormEvent<HTMLFormElement>
    ): Promise<void> => {
        let config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        let data: InternaOtpremnica = {
            brojRadneKnjizice: '',
            datumDokumenta: this.state.datumDokumenta,
            datumValute: new Date(),
            sifraMaloprodaje: this.state.selectedMaloprodaja,
            sifraOtpreme: this.state.selectedOtprema,
            sifraSkladista: this.state.selectedSkladiste,
            sifraOtpremnice: this.state.sifraOtpremnice
        }

        if(!this.props.activeOtpremnica)
        {
            
        await axios.post('http://localhost:4000/otpremnica', data, config)
            .then(async res =>  {
                alert("Opremnica uspesno dodata!")
                let o = await axios.get(`http://localhost:4000/otpremnica/${data.sifraOtpremnice}`)
                this.props.closeModal(o.data);
            })
            .catch(error => {
                alert("GRESKA!")
            })

        }
        else {
            await axios.put('http://localhost:4000/otpremnica', data, config)
            .then(async res =>  {
                alert("Opremnica uspesno izmenjena!")
                let o = await axios.get(`http://localhost:4000/otpremnica/${data.sifraOtpremnice}`)
                this.props.closeModal(o.data);
            })
            .catch(error => {
                alert("GRESKA!")
            })
        }
};



    async componentDidMount() {

        if(this.props.activeOtpremnica){
            console.log('sifra otpremnice je: ' +this.props.activeOtpremnica.sifraOtpremnice )
            this.setState({
                sifraOtpremnice: this.props.activeOtpremnica.sifraOtpremnice,
                datumDokumenta: new Date(this.props.activeOtpremnica.datumDokumenta),
                selectedSkladiste: this.props.activeOtpremnica.sifraSkladista,
                selectedMaloprodaja: this.props.activeOtpremnica.sifraMaloprodaje,
                selectedOtprema: this.props.activeOtpremnica.sifraOtpreme
            })
        }

        await axios.get('http://localhost:4000/skladiste')
            .then(res => {
                console.log(res.data)
                this.setState({
                    skladista: res.data.map((element: Skladiste) =>
                    ({
                        value: element.sifraSkladista,
                        text: element.imeSkladista,
                        key: element.sifraSkladista
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });
        await axios.get('http://localhost:4000/maloprodaja')
            .then(res => {
                console.log(res.data)
                this.setState({
                    maloprodaje: res.data.map((element: Maloprodaja) =>
                    ({
                        value: element.sifraMaloprodaje,
                        text: element.imeMaloprodaje,
                        key: element.sifraMaloprodaje
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });

        await axios.get('http://localhost:4000/otprema')
            .then(res => {
                console.log(res.data)
                this.setState({
                    otpreme: res.data.map((element: Otprema) =>
                    ({
                        value: element.sifraOtpreme,
                        text: element.sifraOtpreme,
                        key: element.sifraOtpreme
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });
    }



    render() {

        return (
            <div>
                <Form id='AddOtpremnicaForm' onSubmit={this.handleSubmitAddOtpremnica}>
                    <Form.Field>
                        <label>Sifra Otpremnice</label>
                        <Input icon='user outline' iconPosition='left' placeholder='SifraOtpremnice' name='sifraOtpremnice' value={this.state.sifraOtpremnice} onChange={this.handleChange} />
                    </Form.Field>
                    <Form.Field>
                        <label>Sifra otpreme</label>
                        <Dropdown
                            placeholder={this.state.otpreme[0]?.text}
                            fluid
                            selection
                            options={this.state.otpreme}
                            name="value"
                            value={this.state.selectedOtprema}
                            onChange={this.dropdownChangeOtprema}
                        >
                        </Dropdown>
                    </Form.Field>

                    <Form.Field>
                        <label>Skladiste</label>
                        <Dropdown
                            placeholder={this.state.skladista[0]?.text}
                            fluid
                            selection
                            options={this.state.skladista}
                            name="value"
                            value={this.state.selectedSkladiste}
                            onChange={this.dropdownChangeSkladiste}
                        >
                        </Dropdown>
                    </Form.Field>
                    <Form.Field>
                        <label>Maloprodaja</label>
                        <Dropdown
                            placeholder={this.state.maloprodaje[0]?.text}
                            fluid
                            selection
                            options={this.state.maloprodaje}
                            name="value"
                            value={this.state.selectedMaloprodaja}
                            onChange={this.dropdownChangeMaloprodaja}
                        >
                        </Dropdown>
                    </Form.Field>

                    <Form.Field>
                        <label>Datum dokumenta</label>
                        <DatePicker
                            dateFormat='dd-MM-yyyy'
                            selected={this.state.datumDokumenta}
                            onChange={(date: any) => this.setState({ datumDokumenta: date })}
                            placeholderText="Click to select date..."
                            maxDate={new Date()}
                        />
                    </Form.Field>
                    <Button id="loginBtn" primary type='submit'>Submit</Button>
                </Form>

            </div >
        );
    }
}

export default AddInternaOtpremnica;