import React from 'react';
//@ts-ignore
import DatePicker from "react-datepicker";
import { Button, Dropdown, Form, Input } from "semantic-ui-react";
import { dbBrocker } from '../db/db-brocker';
import { Maloprodaja } from '../domain/maloprodaja';
import { Skladiste } from '../domain/skladiste';
import SecondaryMenuInternaOtpremnica from './InternaOtpremnicaSecundary';
import axios from 'axios'
import { Otprema } from '../domain/otprema';
import { InternaOtpremnica } from '../domain/internaOtpremnica';
import { throws } from 'node:assert';

interface Otpremnica  {
    sifraOtpremnice: number;
    datumDokumenta: Date;
    sifraSkladista: number;
    sifraMaloprodaje: number;
    sifraOtpreme: number;
    brojRadneKnjizice: string;
    ukupno: number;
}
export type Pdv = {
    sifraPDV: number,
    iznosPDV: number
}
export type Proizvod = {
    sifraProizvoda: number,
    imeProizvoda : string
}

interface IProps {
    closeModal: any,
    activeStavka: Stavka | undefined
    otpremnica: number,
    rb: number
}

interface DropDownItem {
    key: number,
    value: number,
    text: string
}
interface IState {
    redniBroj: number
    sifraOtpremnice: number,
    datumDokumenta: any,
    datumValute: any,
    identificationNumber: string,
    message: string,
    skladista: DropDownItem[],
    maloprodaje: DropDownItem[],
    selectedSkladiste: number,
    selectedMaloprodaja: number,
    otpreme: DropDownItem[],
    selectedOtprema: number,
    proizvodi: DropDownItem[],
    selectedProizvod: number,
    kolicina: number,
    cena: number,
    pdv:DropDownItem [],
    selectedPDV: number,
    rabat: number
}

export type Stavka = {
    redniBroj: number,
    sifraOtpremnice: number,
    datumDokumenta?: Date,
    proizvod?: string,
    kolicina: number,
    cena?: number,
    pdv?: number,
    ukupno?: number,
    rabat?: number,
    sifraProizvoda?: number,
    sifraPDV?: number
}


class AddStavkaInternaOtpremnica extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            rabat: 0,
            selectedPDV: 0,
            cena: 0,
            pdv: [],
            kolicina: 1,
            redniBroj: 1,
            proizvodi: [],
            datumValute: '',
            otpreme: [],
            selectedOtprema: 0,
            skladista: [],
            selectedSkladiste:  0,
            selectedMaloprodaja:0,
            maloprodaje: [],
            sifraOtpremnice:0,
            datumDokumenta: new Date(),
            identificationNumber: '',
            message: '',
            selectedProizvod: 0
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitAddStavka = this.handleSubmitAddStavka.bind(this);
        this.dropdownChangeOtprema = this.dropdownChangeOtprema.bind(this);


    }

    dropdownChangeOtprema = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedOtprema: data.value })
    }
    dropdownChangeProizvod = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedProizvod: data.value })
    }

    dropdownChangePDV = (event: React.SyntheticEvent<HTMLElement>, data: any) => {
        this.setState({ selectedPDV: data.value })
    }


    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        });
    }

    private handleSubmitAddStavka = async (
        e: React.FormEvent<HTMLFormElement>
    ): Promise<void> => {
        let config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        let data: Stavka = {
            kolicina: this.state.kolicina,
            rabat: this.state.rabat,
            sifraPDV: this.state.selectedPDV,
            sifraProizvoda: this.state.selectedProizvod,
            sifraOtpremnice: this.props.otpremnica,
            redniBroj: this.state.redniBroj,
        }

        if(!this.props.activeStavka)
        {
            
        await axios.post(`http://localhost:4000/stavka/`, data, config)
            .then(async res =>  {
                alert("Stavka uspesno dodata!")
                let o = await axios.get(`http://localhost:4000/stavka/${data.sifraOtpremnice}/${data.redniBroj}`)
                this.props.closeModal(o.data);
            })
            .catch(error => {
                alert("GRESKA!")
            })

        }
        else {
            await axios.put('http://localhost:4000/stavka', data, config)
            .then(async res =>  {
                alert("Opremnica uspesno izmenjena!")
                let o = await axios.get(`http://localhost:4000/stavka/${data.sifraOtpremnice}/${data.redniBroj}`)
                this.props.closeModal(o.data);
            })
            .catch(error => {
                alert("GRESKA!")
            })
        }
};



    async componentDidMount() {

        if(this.props.activeStavka){
            console.log('sifra otpremnice je: ' +this.props.activeStavka.sifraOtpremnice )
            this.setState({
                redniBroj: this.props.activeStavka.redniBroj,
                selectedProizvod: this.props.activeStavka.sifraProizvoda? this.props.activeStavka.sifraProizvoda: 0 ,
                kolicina: this.props.activeStavka.kolicina,
                rabat: this.props.activeStavka.rabat?this.props.activeStavka.rabat: 0,
                selectedPDV: this.props.activeStavka.sifraPDV? this.props.activeStavka.sifraPDV: 0 
            })
        }

        await axios.get('http://localhost:4000/proizvod')
            .then(res => {
                console.log(res.data)
                this.setState({
                    proizvodi: res.data.map((element: Proizvod) =>
                    ({
                        value: element.sifraProizvoda,
                        text: element.imeProizvoda,
                        key: element.sifraProizvoda
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });
        await axios.get('http://localhost:4000/pdv')
            .then(res => {
                console.log(res.data)
                this.setState({
                    pdv: res.data.map((element: Pdv) =>
                    ({
                        value: element.sifraPDV,
                        text: element.iznosPDV,
                        key: element.sifraPDV
                    })),
                })
            })
            .catch(error => {
                console.log(error)
            });
     }



    render() {

        return (
            <div>
                <Form id='AddStavkaForm' onSubmit={this.handleSubmitAddStavka}>
                    <Form.Field>
                        <label>Redni broj</label>
                        <Input disabled = {true} icon='user outline' iconPosition='left' placeholder='RedniBroj' name='redniBroj' value={this.props.rb} onChange={this.handleChange} />
                    </Form.Field>
                    <Form.Field>
                        <label>Proizvod</label>
                        <Dropdown
                            placeholder='izaberite proizvod..'
                            fluid
                            selection
                            options={this.state.proizvodi}
                            name="value"
                            value={this.state.selectedProizvod}
                            onChange={this.dropdownChangeProizvod}
                        >
                        </Dropdown>
                    </Form.Field>

                    <Form.Field>
                        <label>Kolicina</label>
                        <Input icon='user outline' iconPosition='left' placeholder='Kolicina' name='kolicina' value={this.state.kolicina} onChange={this.handleChange} />
                    </Form.Field>
                    <Form.Field>
                        <label>PDV</label>
                        <Dropdown
                            placeholder='izaberite pdv..'
                            fluid
                            selection
                            options={this.state.pdv}
                            name="value"
                            value={this.state.selectedPDV}
                            onChange={this.dropdownChangePDV}
                        >
                        </Dropdown>
                    </Form.Field>

                    <Form.Field>
                        <label>Rabat</label>
                        <Input icon='user outline' iconPosition='left' placeholder='Rabat' name='rabat' value={this.state.rabat} onChange={this.handleChange} />
                    </Form.Field>
                    
                    <Button id="loginBtn" primary type='submit'>Submit</Button>
                </Form>

            </div >
        );
    }
}

export default AddStavkaInternaOtpremnica;