import { type } from "os";

export type StavkaInterneOtpremnice = {
  sifraOtpremnice: number;
  ruc: number;
  rednibroj: number;
  kolicina: number;
  sifraPDV: number;
  sifraProizvoda: number;
  datumDokumenta: Date | null;
  iznosPDV: number | null;
};

export type InternaOtpremnica = {
  sifraOtpremnice: number;
  datumDokumenta: Date;
  datumValute: Date;
  sifraSkladista: number;
  sifraMaloprodaje: number;
  sifraOtpreme: number;
  brojRadneKnjizice: string;
};
