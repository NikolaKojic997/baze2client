import React from 'react';
import './App.css';
import "react-datepicker/dist/react-datepicker.css";

import 'semantic-ui-css/semantic.min.css'
import PageHeader from './components/PageHeader';
import AddInternaOtpremnica from './components/AddInternaOtpremnica';
import SelectInternaOtpremnica from './components/SelectInternaOtpremnica';
import SelectStavkaInterneOtpremnice from './components/SelectStavkaInterneOtpremnice';
import SelectKalkulacijaCene from './components/selectKalkulacijaCene';

const BrowserRouter = require("react-router-dom").BrowserRouter;
const Route = require("react-router-dom").Route;
const Link = require("react-router-dom").Link;
const Switch = require("react-router-dom").Switch
const Redirect = require("react-router-dom").Redirect


interface IProps {

}

interface IState {

}


class  App extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props)

    this.handleLogOut = this.handleLogOut.bind(this)
    
  }

  handleLogOut= ()=> {
    this.setState({
        activeProfile: undefined,
        loginSuccess: false
    })
}

    render() {


    return (

        <BrowserRouter>
            <Switch>
                <Route exact  path="/">
                    <PageHeader handleLogOut={this.handleLogOut} activeItem={"InternaOtpremnica"}/>
                    <SelectInternaOtpremnica/>
                </Route>
                <Route exact  path="/InternaOtpremnica">
                    <PageHeader handleLogOut={this.handleLogOut} activeItem={"InternaOtpremnica"}/>
                    <SelectInternaOtpremnica/>
                </Route>
                <Route exact  path="/StavkaInterneOtpremnice">
                    <PageHeader handleLogOut={this.handleLogOut} activeItem={"StavkaInterneOtpremnice"}/>
                    <SelectStavkaInterneOtpremnice/>
                </Route>
                <Route exact  path="/kalkulacijaCene">
                    <PageHeader handleLogOut={this.handleLogOut} activeItem={"kalkulacijaCene"}/>
                    <SelectKalkulacijaCene/>
                </Route>
        </Switch>
        </BrowserRouter>
    )
  }
}

export default App;